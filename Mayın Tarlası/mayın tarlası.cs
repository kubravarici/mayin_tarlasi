﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mayın_Tarlası
{
    class MayınTarlası
    {

        Button[,] kare;
        public Form1 alan;

        int satir;
        int sutun;
        int mayin = 0;
        int[,] sayi;
        int acilan = 0;

        public MayınTarlası(int a, int b, int c)
        {
            satir = a;
            sutun = b;
            mayin = c;
            kare = new Button[satir, sutun];
            sayi = new int[satir, sutun];

        }

        public void TarlaOlustur()
        {

            alan.flowLayoutPanel1.Width = sutun * 30;
            alan.flowLayoutPanel1.Height = satir * 30;
            Form1.ActiveForm.Width = (30 * sutun) + 50;
            Form1.ActiveForm.Height = (30 * satir) + 120;
            alan.flowLayoutPanel1.Controls.Clear();

            for (int i = 0; i < satir; i++)
            {
                for (int j = 0; j < sutun; j++)
                {

                    kare[i, j] = new Button();
                    kare[i, j].Width = 30;
                    kare[i, j].Height = 30;
                    kare[i, j].Margin = new Padding(0);
                    alan.flowLayoutPanel1.Controls.Add(kare[i, j]);
                    kare[i, j].BackColor = Color.SkyBlue;
                    alan.textBox2.Text = mayin.ToString();
                    kare[i, j].Tag = "0";
                    kare[i, j].MouseDown += new MouseEventHandler(mayın_tarlası_Click);
                    kare[i, j].Click += mayın_tarlası_Click;

                }
            }
        }

        public void Hesapla()
        {
            for (int i = 0; i < satir; i++)
            {
                for (int j = 0; j < sutun; j++)
                {
                    if (kare[i, j].Enabled == false)
                    {
                        acilan++;
                    }
                }
            }

            if (alan.saniye != 0)
            {
                float skor = (acilan - 1) / ((alan.dakika * 60) + alan.saniye);
                alan.textBox3.Text = skor.ToString();
            }
        }

        public void OyunBittiKontrol()
        {
            for (int i = 0; i < satir; i++)
            {
                for (int j = 0; j < sutun; j++)
                {
                    if (kare[i, j].Enabled == false)
                    {
                        acilan++;
                    }
                }
            }

            if (acilan == (satir * sutun) - mayin)
            {
                acilan = 0;
                alan.timer1.Stop();
                acilan = acilan + 1;
                Hesapla();
                DialogResult sonuc = MessageBox.Show("Oyunu Kazandınız :))\nskor : " + alan.textBox3.Text + "\nYeniden oynamak ister misiniz ?", "Oyun", MessageBoxButtons.YesNo);

                if (sonuc == DialogResult.Yes)
                {
                    alan.flowLayoutPanel1.Controls.Clear();
                    alan.textBox1.Text = "0";

                    alan.textBox2.Text = "0";
                    alan.saniye = 0;
                    alan.dakika = 0;
                }

                else
                {
                    alan.Close();
                }
            }

            else
            {
                acilan = 0;
            }

        }

        private void BosAc(int i, int j)
        {
            if (kare[i, j].Tag.ToString() == "0" && kare[i, j].BackgroundImage == null)
            {

                kare[i, j].BackColor = Color.Snow;
                kare[i, j].Enabled = false;
                kare[i, j].Tag = "10";

                if (j < sutun - 1)
                {
                    BosAc(i, j + 1);
                }
                if (j > 0)
                {
                    BosAc(i, j - 1);
                }

                if (i < satir - 1)
                {
                    BosAc(i + 1, j);
                }
                if (i > 0)
                {
                    BosAc(i - 1, j);
                }
                if (i > 0 && j < sutun - 1)
                {
                    BosAc(i - 1, j + 1);
                }
                if (i < satir - 1 && j < sutun - 1)
                {
                    BosAc(i + 1, j + 1);
                }
                if (i < satir - 1 && j > 0)
                {
                    BosAc(i + 1, j - 1);
                }
                if (i > 0 && j > 0)
                {
                    BosAc(i - 1, j - 1);
                }

            }
            else if (kare[i, j].Tag.ToString() == "1")
            {

            }
            else if (kare[i, j].Tag.ToString() == "-1" && kare[i, j].BackgroundImage == null)
            {
                kare[i, j].Text = sayi[i, j].ToString();
                kare[i, j].Enabled = false;
                kare[i, j].BackColor = Color.Snow;
                kare[i, j].Tag = "10";
            }

        }

        private void mayın_tarlası_Click(object sender, MouseEventArgs e)
        {
            Button tiklanan = (Button)sender;

            int tiklanan_i = 1;
            int tiklanan_j = 1;

            for (int i = 0; i < satir; i++)
            {
                for (int j = 0; j < sutun; j++)
                {
                    if (tiklanan == kare[i, j])
                    {
                        tiklanan_i = i;
                        tiklanan_j = j;
                    }
                }
            }

            if (e.Button == MouseButtons.Right)
            {

                if (kare[tiklanan_i, tiklanan_j].BackgroundImage != null)
                {
                    kare[tiklanan_i, tiklanan_j].BackgroundImage = null;
                    kare[tiklanan_i, tiklanan_j].BackColor = Color.SkyBlue;
                    mayin += 1;
                    alan.textBox2.Text = mayin.ToString();
                }
                else
                {
                    if (mayin <= 0)
                    {
                        MessageBox.Show("\tHATA!\n\nMayın sayısı kadar bayrak ekleyebiirsiniz..");
                    }
                    else
                    {
                        kare[tiklanan_i, tiklanan_j].BackColor = Color.Snow;
                        kare[tiklanan_i, tiklanan_j].BackgroundImage = Properties.Resources.flag;
                        kare[tiklanan_i, tiklanan_j].BackgroundImageLayout = ImageLayout.Stretch;

                        mayin -= 1;
                        alan.textBox2.Text = mayin.ToString();
                    }
                }
            }
        }

        void mayın_tarlası_Click(object sender, EventArgs e)
        {
            Button tiklanan = (Button)sender;
            int gelen_i = 1;
            int gelen_j = 1;

            for (int i = 0; i < satir; i++)
            {
                for (int j = 0; j < sutun; j++)
                {
                    if (tiklanan == kare[i, j])
                    {
                        gelen_i = i;
                        gelen_j = j;
                    }
                }
            }

            if (kare[gelen_i, gelen_j].Tag.ToString() == "-1" && kare[gelen_i, gelen_j].BackgroundImage == null)
            {
                kare[gelen_i, gelen_j].Enabled = false;
                kare[gelen_i, gelen_j].Text = sayi[gelen_i, gelen_j].ToString();
                kare[gelen_i, gelen_j].BackColor = Color.Snow;
                kare[gelen_i, gelen_j].Tag = "10";
                OyunBittiKontrol();
            }
            else if (kare[gelen_i, gelen_j].Tag.ToString() == "0" && kare[gelen_i, gelen_j].BackgroundImage == null)
            {
                BosAc(gelen_i, gelen_j);
                OyunBittiKontrol();
            }
            else if (kare[gelen_i, gelen_j].BackgroundImage == null)
            {
                for (int i = 0; i < satir; i++)
                {
                    for (int j = 0; j < sutun; j++)
                    {

                        if (kare[i, j].Tag.ToString() == "1")
                        {
                            kare[gelen_i, gelen_j].Enabled = false;
                            kare[i, j].BackgroundImage = Properties.Resources.bomb;
                            kare[i, j].BackgroundImageLayout = ImageLayout.Stretch;
                            alan.timer1.Stop();

                            if (kare[i, j].BackgroundImage == Properties.Resources.flag && kare[i, j].Tag.ToString() == "1")
                            {
                                kare[i, j].BackgroundImage = Properties.Resources.images;
                            }

                        }
                    }
                }

                Hesapla();
                DialogResult sonuc = MessageBox.Show("süre : " + alan.textBox1.Text + "\nskor : " + alan.textBox3.Text + "\nKaybettiniz. Yeniden oynamak ister misiniz ?", "Oyun", MessageBoxButtons.YesNo);

                if (sonuc == DialogResult.Yes)
                {
                    alan.flowLayoutPanel1.Controls.Clear();
                    alan.textBox1.Text = "0";
                    alan.saniye = 0;
                    alan.dakika = 0;
                    alan.textBox2.Text = "0";
                    alan.textBox3.Text = "0";
                }
                else
                {
                    alan.Close();
                }
            }
        }

        public void MayinlariBelirle()
        {
            Random rnd = new Random();

            for (int i = 0; i < mayin; i++)
            {
                int x, y;
                x = rnd.Next(0, satir);
                y = rnd.Next(0, sutun);
                while (MineControl(x, y))
                {
                    x = rnd.Next(0, satir);
                    y = rnd.Next(0, sutun);
                }

                kare[x, y].Tag = "1";

            }
        }

        public bool MineControl(int x, int y)
        {

            if (kare[x, y].Tag.ToString() == "1")
            {
                return true;
            }

            return false;

        }
        public void Sayidagit()
        {

            for (byte i = 0; i < satir; i++)
                for (byte j = 0; j < sutun; j++)
                {
                    if (kare[i, j].Tag.ToString() != "1")
                    {

                        if (j % sutun != 0)//ilk sutunda değilse
                            if (kare[i, j - 1].Tag.ToString() == "1")//sol
                            {
                                sayi[i, j]++;
                            }

                        if ((j + 1) % sutun != 0) // son sutunda değilse
                            if (kare[i, j + 1].Tag.ToString() == "1")//sağ
                            {
                                sayi[i, j]++;
                            }

                        if (i % satir != 0)//ilk satırda değilse
                            if (kare[i - 1, j].Tag.ToString() == "1")//üst
                            {
                                sayi[i, j]++;
                            }

                        if (i < satir - 1)// son satırda değilse
                            if (kare[i + 1, j].Tag.ToString() == "1")//alt
                            {
                                sayi[i, j]++;
                            }

                        if ((i % satir != 0) && (j % sutun != 0)) //ilk satır ve ilk sutunda değilse
                            if (kare[i - 1, j - 1].Tag.ToString() == "1")//solüst
                            {
                                sayi[i, j]++;

                            }

                        if ((i % satir != 0) && ((j + 1) % sutun != 0)) //ilk satır ve son sutunda değilse
                            if (kare[i - 1, j + 1].Tag.ToString() == "1")//sağüst
                            {
                                sayi[i, j]++;
                            }

                        if ((j % sutun != 0) && (i < satir - 1)) //ilk sutun ve son satırda değilse
                            if (kare[i + 1, j - 1].Tag.ToString() == "1")//solalt
                            {
                                sayi[i, j]++;
                            }

                        if (((j + 1) % sutun != 0) && (i < satir - 1)) //son satır ve son sutunda değilse
                            if (kare[i + 1, j + 1].Tag.ToString() == "1")//sağalt
                            {
                                sayi[i, j]++;
                            }

                        if (sayi[i, j] != 0)
                        {
                            kare[i, j].Tag = "-1";
                        }

                    }
                }
        }
    }
}




