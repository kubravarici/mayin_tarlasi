﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mayın_Tarlası
{
    public partial class Form1 : Form
    {
        public int saniye = 0;
        public int dakika = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void başlangıçToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MayınTarlası tarla = new MayınTarlası(9, 9, 10);

            tarla.alan = this;
            tarla.TarlaOlustur();
            tarla.MayinlariBelirle();
            tarla.Sayidagit();
            timer1_Tick(sender, e);
            Gosterme();

        }

        private void ortaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MayınTarlası tarla = new MayınTarlası(16, 16, 40);

            tarla.alan = this;
            tarla.TarlaOlustur();
            tarla.MayinlariBelirle();
            tarla.Sayidagit();
            timer1_Tick(sender, e);
            Gosterme();
        }

        private void zorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MayınTarlası tarla = new MayınTarlası(16, 30, 99);

            tarla.alan = this;
            tarla.TarlaOlustur();
            tarla.MayinlariBelirle();
            tarla.Sayidagit();
            timer1_Tick(sender, e);
            Gosterme();
        }

        private void çıkışToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak İstediğinizden Emin misiniz?", "Onay", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();

            saniye++;

            if (saniye == 60)
            {
                saniye = 0;
                dakika++;
            }

            textBox1.Text = dakika + ":" + saniye;
        }

        private void yeniOyunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            saniye = 0;
            dakika = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Gosterme();

            int sayi1 = Convert.ToInt32(comboBox1.SelectedItem);
            int sayi2 = Convert.ToInt32(comboBox2.SelectedItem);
            int sayi3 = Convert.ToInt32(comboBox3.SelectedItem);
            MayınTarlası tarla = new MayınTarlası(sayi1, sayi2, sayi3);
            Form1.ActiveForm.Width = (30 * sayi1) + 50;
            Form1.ActiveForm.Height = (30 * sayi2) + 120;
            tarla.alan = this;
            tarla.TarlaOlustur();
            tarla.MayinlariBelirle();
            tarla.Sayidagit();
            timer1_Tick(sender, e);

        }

        private void özelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1.ActiveForm.Width = 350;
            Form1.ActiveForm.Height = 370;
            flowLayoutPanel1.Controls.Clear();
            flowLayoutPanel1.Visible = false;
            comboBox1.Visible = true;
            comboBox2.Visible = true;
            comboBox3.Visible = true;
            button1.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;

        }
        private void Gosterme()
        {
            comboBox1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            button1.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            flowLayoutPanel1.Visible = true;
        }
    }
}
